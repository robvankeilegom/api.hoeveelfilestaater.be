<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = [
        'm', 'recorded', 'm',
    ];

    protected $dates = [
        'recorded',
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function getKmAttribute()
    {
        return $this->m / 1000;
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone('Europe/Brussels');
    }
}
